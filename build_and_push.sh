#!/bin/bash

IMAGES=("base-image" "base-clang" "base-runner" "base-builder")
LOGFILE=base-images.log

if [ "${DOCKER_REGISTRY}" = "" ]
then
  if [ "${1}" = "" ]
  then
    echo "[!] Please set DOCKER_REGISTRY or use ${0} <regsitry path>"
    exit 1
  fi
  DOCKER_REGISTRY="${1}"
fi

echo "[+] Logging in ..."
docker login "${DOCKER_REGISTRY}"
if [ "$?" != "0" ]
then 
  echo "[!] Could not log in to ${DOCKER_REGISTRY}!"
  exit 1
fi

rm -f ${LOGFILE}
for img in ${IMAGES[@]}
do
  echo "Building ${img}" | tee ${LOGFILE}
  docker build --build-arg DOCKER_REGISTRY="${DOCKER_REGISTRY}" -t ${DOCKER_REGISTRY}/${img} infra/base-images/${img} >> ${LOGFILE}
  if [ "$?" != "0" ]
  then 
    echo "[!] Could not build ${DOCKER_REGISTRY}/${img}!"
    exit 1
  fi
done

for img in ${IMAGES[@]}
do
  echo "Pushing ${img}" | tee ${LOGFILE}
  docker push ${DOCKER_REGISTRY}/${img} >> ${LOGFILE}
  if [ "$?" != "0" ]
  then 
    echo "[!] Could not push ${DOCKER_REGISTRY}/${img}!"
    exit 1
  fi
done

echo "[+] ALL DONE!"
